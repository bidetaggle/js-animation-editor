import {debug, monitor} from './log.js';
import {render, update} from './render.js';

(function() {
    const WIDTH = 1600;
    const HEIGHT = 900;

    var Image = function(imgTag){
        this.dom = imgTag;
        this.x = 0;
        this.y = 0;
        this.width = imgTag.width;
        this.height = imgTag.height;

        this.draw = function(){
            ctx.drawImage(this.dom, this.x, this.y, img.width, img.height);
        }

        this.resize = function(percentage){
            this.width = this.width * percentage / 100;
            this.height = this.height * percentage / 100;
        };

        this.center = function(){
            this.x = canvas.width/2 - this.width/2;
            this.y = canvas.height/2 - this.height/2;
        }

        this.move = function(x, y, speed){
            this.x += speed;
            this.y += speed;
        }
    }



    debug("starting...");
    var canvas = document.getElementById("canvas");
    canvas.width = WIDTH;
    canvas.height = HEIGHT;
    var ctx = canvas.getContext("2d");
    debug("Ready.");

    var img = new Image(document.getElementsByTagName("img")[0]);

    //img.center();
    //img.draw();

    // function render(tFrame){
    //     monitor("tFrame",`${tFrame}`);
    //     monitor("lastTick",`${anim.lastTick}`);
    //     monitor("tFrame - lastTick",`${tFrame - anim.lastTick}`);
    // }
    //
    // function update(lastTick){
    //     //debug("lastTick: " +lastTick);
    //
    // }

    var anim = {};

    function main( tFrame ) {
        anim.stopMain = window.requestAnimationFrame( main );
        var nextTick = anim.lastTick + anim.tickLength;
        var numTicks = 0;

        //If tFrame < nextTick then 0 ticks need to be updated (0 is default for numTicks).
        //If tFrame = nextTick then 1 tick needs to be updated (and so forth).
        //Note: As we mention in summary, you should keep track of how large numTicks is.
        //If it is large, then either your game was asleep, or the machine cannot keep up.
        if (tFrame > nextTick) {
            var timeSinceTick = tFrame - anim.lastTick;
            numTicks = Math.floor( timeSinceTick / anim.tickLength );
        }

        queueUpdates( numTicks );
        render( tFrame, numTicks );
        anim.lastRender = tFrame;
    }

    function queueUpdates( numTicks ) {
        for(var i=0; i < numTicks; i++) {
            anim.lastTick = anim.lastTick + anim.tickLength; //Now lastTick is this tick.
            update( anim.lastTick );
        }
    }

    anim.lastTick = performance.now();
    anim.lastRender = anim.lastTick; //Pretend the first draw was on first update.
    anim.tickLength = 50; //This sets your simulation to run at 20Hz (50ms)

    main(performance.now()); // Start the cycle

})();
