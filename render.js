import {debug, monitor} from './log.js';


export function render(tFrame, numTicks){
    monitor("tFrame",`${tFrame}`);
    monitor("numTicks",`${numTicks}`);
}

export function update(lastTick){
    //debug("lastTick: " +lastTick);
}
