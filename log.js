export function debug(message){
    document.getElementById("debug").innerHTML += `${message}<br />`;
}

var monitoringTools = [];
export function monitor(tool, message){
    var i;
    let passed = false;
    for (i = 0 ; i<monitoringTools.length ; i++){
        if(monitoringTools[i].name == tool)
        {
            passed = true;
            monitoringTools[i].message = message;
        }
    }
    if(passed == false){
        monitoringTools.push({name: tool, message: message});
    }

    let monitor = document.getElementById("monitor");

    monitor.innerHTML = ``;
    for (var i = 0 ; i<monitoringTools.length ; i++){
        $("#monitor").append(`<div>${monitoringTools[i].name} : ${monitoringTools[i].message}</div>`);
    }
}
